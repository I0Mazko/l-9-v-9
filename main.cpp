﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;



int main()
{
	sf::RenderWindow window(sf::VideoMode(1000, 1000), "Lab9 V9");

	int rectangle_y = 900;
	int rectangle_x = 800;
	int rectangle_z = 700;


	sf::RectangleShape rectangle(sf::Vector2f(10.f, 20.f));
	rectangle.setFillColor(sf::Color(204, 48, 208));
	rectangle.setOrigin(100, 25);
	rectangle.setPosition(rectangle_x, 250);


	sf::RectangleShape rectangle1(sf::Vector2f(35.f, 35.f));
	rectangle1.setFillColor(sf::Color(12, 198, 237));
	rectangle1.setOrigin(100, 50);
	rectangle1.setPosition(rectangle_x, 250);

	sf::RectangleShape rectangle2(sf::Vector2f(50.f, 50.f));
	rectangle2.setFillColor(sf::Color(255, 0, 0));
	rectangle2.setOrigin(100, 100);
	rectangle2.setPosition(rectangle_x, 250);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		if (rectangle_y > 100)
		{
			rectangle.setPosition(rectangle_y, 100);
			rectangle_y = rectangle_y - 1;
		}


		if (rectangle_x > 100)
		{
			rectangle1.setPosition(rectangle_x, 250);
			rectangle_x = rectangle_x - 2;
		}

		if (rectangle_z > 85)
		{
			rectangle2.setPosition(rectangle_z, 400);
			rectangle_z = rectangle_z - 3;
		}


		window.clear(sf::Color(120, 120, 120, 0));

		window.draw(rectangle);
		window.draw(rectangle1);
		window.draw(rectangle2);


		window.display();

		std::this_thread::sleep_for(10ms);
	}

	return 0;
}